drop database if exists quanlysanpham;
create database if not exists quanlysanpham;
use quanlysanpham;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlysanpham
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `matkhau` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taikhoan_UNIQUE` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','1234'),(2,'admin1','12345');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitietdonhang`
--

DROP TABLE IF EXISTS `chitietdonhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitietdonhang` (
  `madon` int(11) NOT NULL,
  `masp` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  PRIMARY KEY (`madon`,`masp`),
  KEY `fk_chitietdonhang_sanpham_idx` (`masp`),
  CONSTRAINT `fk_chitietdonhang_donhang` FOREIGN KEY (`madon`) REFERENCES `donhang` (`madon`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_chitietdonhang_sanpham` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietdonhang`
--

LOCK TABLES `chitietdonhang` WRITE;
/*!40000 ALTER TABLE `chitietdonhang` DISABLE KEYS */;
INSERT INTO `chitietdonhang` VALUES (4,1,3),(4,3,3),(5,1,3),(5,3,3),(6,1,3),(6,3,3),(7,1,3),(7,3,3),(8,1,3),(8,3,3),(9,1,3),(9,3,3),(10,1,3),(10,3,3),(11,1,3),(11,3,3),(12,1,3),(12,3,3),(13,1,3),(13,3,3),(14,1,3),(14,3,3),(15,1,3),(15,3,3),(16,1,3),(16,3,3),(17,1,3),(17,3,3),(18,3,2),(19,2,2),(20,1,2),(21,2,1),(21,3,1),(22,1,1),(23,1,1),(23,2,1),(24,1,1),(25,1,1);
/*!40000 ALTER TABLE `chitietdonhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donhang` (
  `madon` int(11) NOT NULL AUTO_INCREMENT,
  `makh` int(11) NOT NULL,
  `tennguoinhan` varchar(50) CHARACTER SET utf8 NOT NULL,
  `diachinhan` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sodienthoai` varchar(15) NOT NULL,
  `ghichu` text,
  `ngaymua` date NOT NULL,
  `tinhtrang` char(1) NOT NULL,
  PRIMARY KEY (`madon`),
  KEY `fk_donhang_khachhang_idx` (`makh`),
  CONSTRAINT `fk_donhang_khachhang` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donhang`
--

LOCK TABLES `donhang` WRITE;
/*!40000 ALTER TABLE `donhang` DISABLE KEYS */;
INSERT INTO `donhang` VALUES (1,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(2,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(3,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(4,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(5,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(6,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(7,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(8,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(9,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(10,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(11,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(12,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(13,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(14,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(15,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(16,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(17,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(18,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(19,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(20,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(21,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(22,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(23,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(24,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0'),(25,10,'Phat Truong','Đường 25, q7','1234','','2018-06-29','0');
/*!40000 ALTER TABLE `donhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `giohang`
--

DROP TABLE IF EXISTS `giohang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `giohang` (
  `makh` int(11) NOT NULL,
  `masp` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  PRIMARY KEY (`makh`,`masp`),
  KEY `fk_giohang_sanpham_idx` (`masp`),
  CONSTRAINT `fk_giohang_khachhang` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_giohang_sanpham` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giohang`
--

LOCK TABLES `giohang` WRITE;
/*!40000 ALTER TABLE `giohang` DISABLE KEYS */;
/*!40000 ALTER TABLE `giohang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hangsanxuat`
--

DROP TABLE IF EXISTS `hangsanxuat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hangsanxuat` (
  `mahangsx` int(11) NOT NULL AUTO_INCREMENT,
  `ten` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`mahangsx`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hangsanxuat`
--

LOCK TABLES `hangsanxuat` WRITE;
/*!40000 ALTER TABLE `hangsanxuat` DISABLE KEYS */;
INSERT INTO `hangsanxuat` VALUES (1,'Adidas'),(2,'Coolbell'),(3,'Toppu'),(4,'Nike'),(5,'The North Face'),(6,'Sakos'),(7,'Mikko'),(8,'Agva'),(9,'Mountainsmith'),(10,'Seliux'),(11,'Simplecarry'),(12,'Crumpler'),(13,'Herringbone'),(14,'Puma'),(15,'SRSLY'),(16,'Kakashi'),(17,'Rivacase'),(18,'Tucano'),(19,'Case Logic');
/*!40000 ALTER TABLE `hangsanxuat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hinhanh`
--

DROP TABLE IF EXISTS `hinhanh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hinhanh` (
  `masp` int(11) NOT NULL,
  `linkanh` varchar(50) NOT NULL,
  PRIMARY KEY (`masp`,`linkanh`),
  CONSTRAINT `fk_hinhanh_sanpham` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hinhanh`
--

LOCK TABLES `hinhanh` WRITE;
/*!40000 ALTER TABLE `hinhanh` DISABLE KEYS */;
/*!40000 ALTER TABLE `hinhanh` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `khachhang`
--

DROP TABLE IF EXISTS `khachhang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `khachhang` (
  `makh` int(11) NOT NULL AUTO_INCREMENT,
  `ten` varchar(50) CHARACTER SET utf8 NOT NULL,
  `gioitinh` int(11) NOT NULL,
  `diachi` varchar(100) CHARACTER SET utf8 NOT NULL,
  `sodienthoai` varchar(15) NOT NULL,
  `matkhau` varchar(256) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`makh`),
  UNIQUE KEY `sodienthoai_UNIQUE` (`sodienthoai`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `khachhang`
--

LOCK TABLES `khachhang` WRITE;
/*!40000 ALTER TABLE `khachhang` DISABLE KEYS */;
INSERT INTO `khachhang` VALUES (1,'Nguyen A',1,'102 ABC, CDE','0901421492','1234','abc@gmail.com'),(2,'Nguyen B',0,'32, NCB, SLX','0124831254','1234','axe@gmail.com'),(3,'Phat Truong',1,'fdsg fsgdf1','1111','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','0907452011'),(4,'Phat Truong',1,'11','0907452011','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','truongtanphat23997@gmail.com'),(5,'Phat Truong',1,'Đường 25, q7','015446','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','truongtanphat23997@gmail.com'),(6,'aaaa',1,'Đường 25, q7','123456','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','aaaaaa'),(7,'Sơn',1,'Đường 25, q7','0121751249','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','son@gmail.com'),(8,'Phúc',1,'Đường 25, q7','123456789','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','phuc123@gmail.com'),(9,'Phat Truong',1,'Đường 25, q7','123','e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855','truongtanphat23997@gmail.com'),(10,'Phat Truong',1,'Đường 25, q7','1234','03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4','truongtanphat23997@gmail.com');
/*!40000 ALTER TABLE `khachhang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loaisp`
--

DROP TABLE IF EXISTS `loaisp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loaisp` (
  `maloai` int(11) NOT NULL AUTO_INCREMENT,
  `tenloai` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`maloai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loaisp`
--

LOCK TABLES `loaisp` WRITE;
/*!40000 ALTER TABLE `loaisp` DISABLE KEYS */;
INSERT INTO `loaisp` VALUES (1,'balo'),(2,'tuixach');
/*!40000 ALTER TABLE `loaisp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sanpham`
--

DROP TABLE IF EXISTS `sanpham`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sanpham` (
  `masp` int(11) NOT NULL AUTO_INCREMENT,
  `loai` int(11) NOT NULL,
  `ten` varchar(200) CHARACTER SET utf8 NOT NULL,
  `gia` int(11) NOT NULL,
  `soluongcon` int(11) DEFAULT NULL,
  `soluongban` int(11) DEFAULT NULL,
  `luotxem` int(11) DEFAULT NULL,
  `hangsx` int(11) NOT NULL,
  `mota` text CHARACTER SET utf8 NOT NULL,
  `ngaynhaphang` date NOT NULL,
  `linkanh` varchar(50) NOT NULL,
  PRIMARY KEY (`masp`),
  KEY `fk_sanpham_loai_idx` (`loai`),
  KEY `fk_sanpham_hangsx_idx` (`hangsx`),
  CONSTRAINT `fk_sanpham_hangsx` FOREIGN KEY (`hangsx`) REFERENCES `hangsanxuat` (`mahangsx`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sanpham_loai` FOREIGN KEY (`loai`) REFERENCES `loaisp` (`maloai`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sanpham`
--

LOCK TABLES `sanpham` WRITE;
/*!40000 ALTER TABLE `sanpham` DISABLE KEYS */;
INSERT INTO `sanpham` VALUES (1,1,'Adidas BA001',300000,27,29,249,1,'Balo Adidas BA001 là dòng balo của thương hiệu Adidas với thiết kế lạ mắt với nhiều mắc sắc sặc sỡ tạo nên phong cách độc đáo lạ mắt phù hợp với mọi lứa tuổi giới tính.','2018-06-18','/images/balo/b1.jpg'),(2,1,'Adidas BA049',385000,42,3,34,1,'Hàng đảm bảo chất lượng tốt. Xuất xứ: Việt Nam. Chất liệu: Vải D600 đã qua xử lý nhiệt ép. Kích thước: 30cm x 15cm x 45cm. Để được laptop 15.5 inch, thích hợp cho học sinh, sinh viênCó nhiều ngăn chứa đồ tiện lợi. Màu sắc: xám phối đen. Đường may tỉ mỉ chắc chắnThích hợp cho cả nam lẫn nữ, thiết kế hiện đại trẻ trung.','2018-06-28','/images/balo/b2.jpg'),(3,1,'Adidas Classic BA436',350000,25,14,381,1,'Vải bố cao cấp, bền bỉ. Có đệm chống shock laptop. Đựng vừa laptop 15inch. Hợp thời trang, năng động, trẻ trung.','2018-06-19','/images/balo/b3.jpg'),(4,1,'Adidas Classic BA512',320000,18,23,431,1,'Kích thước: 30 x 47 x 15cm. Chất liệu: 100% Polyester, chống thấm tốt. Một ngăn riêng cho laptop đến 15,6″ + 1 ngăn lớn để đồ dùng vật dụng. Phần đáy bằng giả da chống nước tuyệt đối.','2018-06-01','/images/balo/b4.jpg'),(5,1,'Adidas originals trefoil',300000,40,14,261,1,'Balo Adidas Originals Trefoi là dòng balo của thương hiệu Adidas với thiết kế lạ mắt tạo nên phong cách độc đáo lạ mắt phù hợp với mọi lứa tuổi giới tính.Balo Adidas Originals Trefoil với kích thước 45cm x 30cm x 10cm được thiết kế với một ngăn chính lớn và một ngăn đựng laptop lên tới 15 inch riêng biệt với lớp đệm chống sốc giúp bảo vệ tối đa laptop của bạn.ược làm 100% từ polyester sử dụng 2 lớp vải giúp balo chống thấm nước và chống bám bụi cực kì hiệu quả. Cùng với đó là quai đeo được trang bị lớp đêm giúp giảm áp lực lên vai giúp bạn thoải mái trong quá trình di chuyển mang theo những vật dụng nặng- thiết kế lưới điều hòa không khí giúp tăng khả năng thoát mồ hôi.','2018-06-13','/images/balo/b5.jpg'),(6,1,'Coolbell cb6206',420000,25,6,164,2,'Balô Coolbell 6206 14.6\" Được thiết kế tay nhôm. Kiểu dáng thời trang, thanh lịch. Có khả năng chịu nước, chống bám bụi. Có nhiều màu sắc cho bạn lựa chọn. Kích thước: 14 - 15 inch. Chất liệu : Vải chống thấm nước. Ngăn laptop; chống sốc tổ ong dạng hạt.','2018-06-11','/images/balo/b6.jpg'),(7,1,'Coolbell cb2057',480000,36,46,91,2,'Balo coolbell 2057: Mẫu mới nhất dùng vừa cho laptop 15.6\", có một khoang máy tính chuyên nghiệp. Thiết kế đẹp mắt quai đeo chắc chắn. Màu sắc : Đen, ghi, xanh than, tím. Chất liệu : Vải chống thấm nước.','2018-06-21','/images/balo/b7.jpg'),(8,1,'Coolbell cb6707',480000,17,22,106,2,'Balo coolbell - 6707 đựng laptop tối đa 17 inch Sản phẩm thời trang công sở rất lịch sự. Kích thước: 50 x 34 x 17cm. Chất liệu: Nylon Cấu trúc.','2018-06-23','/images/balo/b8.jpg'),(9,1,'Coolbell cb8005',680000,51,3,67,2,'CB 8005 là dòng sản phẩm balo thời trang chính hãng được thương hiệu balo quốc tế Coolbell cho ra mắt trên thị trường thời gian gần đây. Dòng sản phẩm này có thiết kế trẻ trung đẹp mắt, phù hợp với nhiều đối tượng người sử dụng. Nếu bạn đang cần tìm một chiếc balo laptop 14.4 inch vừa có thể phục vụ cho học tập, công việc thì CB 6206 là lựa chọn hoàn hảo. Kích thước: 40x30x14cm, phù hợp với máy tính xách tay 14.4\'\'. Chất liệu: vải 210D polyester trượt nước và chống bám bụi. Màu sắc: Đen, Ghi, xanh jeans, xanh navy, xám.','2018-06-28','/images/balo/b9.jpg'),(10,1,'Coolbell cb3309',590000,16,15,315,2,'Balo Laptop coolbell 3309 là sản phẩm thời trang nhưng rất lịch sự. Balo đựng laptop tối đa : 15.6 inch Chất liệu balo chịu nước và chống bụi bẩn tốt. Màu sắc : Đen, nâu, xám. Chất liệu : Vải chống thấm nước. Ngăn laptop; chống sốc tốt.','2018-06-08','/images/balo/b10.jpg'),(11,1,'Toppu tp484',990000,2,46,679,3,'The Toppu TP-484 mang phong cách trẻ trung, thời trang. Đựng vừa laptop: 15\". Màu sắc: Đen / Nâu. Kích thước: Dài 30 cm x Cao 40 cm x Rộng 12 cm. Chất liệu vải: Da tổng hợp cao cấp. ','2018-05-29','/images/balo/b11.jpg'),(12,1,'Toppu tp488',1050000,34,11,343,3,'Toppu TP 488 kiểu dáng hộp gọn gàng, với rất nhiều ngăn giúp việc để đồ đạc thoải mái. Chất vải Polyester chống thấm nước, độ bền bỉ cao, quai đeo chắc chắn với quai trợ lực. Đựng vừa laptop: 15.6″ trở xuống, có chống sốc tốt. Kích thước:  Cao 41 cm x Dài 25 cm x Rộng 15 cm. Chất liệu vải: Polyester cao cấp, trượt nước tốt.','2018-06-14','/images/balo/b12.jpg'),(13,1,'Toppu tp390',1150000,14,35,163,3,'Toppu TP 390 kiểu dáng rất sang trọng, mang đậm phong cách Hàn Quốc. Các ngăn được bố trí rất khoa học, với ngăn laptop chống sốc tốt, và nhiều ngăn để đồ linh tinh. Chất vải là Nylon 1680D cao cấp nhất cho các dòng balo, siêu bền bỉ, mặt vải mịn màng, chống thấm nước tuyệt đối. Kích thước:  Cao 48 cm x Dài 28 cm x Rộng 14 cm. Chất liệu vải: Vải Nylon 1680D cao cấp.','2018-06-17','/images/balo/b13.jpg'),(14,1,'Toppu tp301',900000,36,11,77,3,'Toppu TP 301 kiểu dáng rất sang trọng, mang đậm phong cách Hàn Quốc. Có quai đeo kiểu balo, và cũng có thể giấu quai balo vào phía trong để xách như cặp xách, và cũng có quai đeo chéo rất tiện lợi. Chất vải là Nylon 1680D cao cấp nhất cho các dòng balo, siêu bền bỉ, mặt vải mịn màng, chống thấm nước tuyệt đối. ','2018-06-26','/images/balo/b14.jpg'),(15,1,'Toppu tp515N Black',900000,42,7,314,3,'Kích thước:  Cao 39 cm x Dài 30 cm x Rộng 10 cm. Chất liệu vải: Vải Nylon 1680D cao cấp.','2018-06-27','/images/balo/b15.jpg'),(16,1,'Nike vapor energy',750000,38,31,134,4,'Ba lô Nike Vapor Energy Training là vật dụng lý tưởng cho bạn khi đến các phòng tập hay thực hiện các hoạt động ngoài trời. Với thiết kế mang phong cách thể thao, chắc chắn, bền và tiện dụng sẽ cho bạn một cảm giác thật dự tự tin khi ra ngoài. Kích thước: 52x 30,5 x 19 (cm). Chất liệu: 100% polyeste.','2018-05-28','/images/balo/b16.jpg'),(17,1,'zwarNike sport zak',670000,15,15,631,4,'Balo Nike Zwart Sport là dòng balo thể thao được Nike ra mắt mới đây. Với chất liệu 1005 Polyester chống thấm nước, quai đeo có đệm giảm sức nặng khi mang. Logo Nike in to ở trước tăng tính thời trang cho sản phẩm. Cao 47 cm x Rộng 34 cm x Dài 13 cm.','2018-06-13','/images/balo/b17.jpg'),(18,1,'Nike Lebron Max Air Ambassador',1400000,9,34,162,4,'Kiểu dáng thể thao – Thiết kế mới nhất của Nike Lebron Max Air Ambassador – Vừa có thể đeo như balo vừa có thể xách như túi trống thể thao thật tiện dụng. Chất liệu 100% Polyester – Chống thấm tuyệt đối. Kích thước: 30 x 60 x 48 cm.','2018-06-02','/images/balo/b18.jpg'),(19,1,'Nike mochila classic turf bp',380000,22,19,346,4,'Với chất liệu 1005 Polyester chống thấm nước, quai đeo có đệm giảm sức nặng khi mang. Logo Nike in to ở trước tăng tính thời trang cho sản phẩm. Balo bao gồm 2 ngăn chính siêu rộng 25L , ngăn phụ ở trước cũng rộng không kém. Cao 47 cm x Rộng 34 cm x Dài 13 cm.','2018-06-14','/images/balo/b19.jpg'),(20,1,'Nike ba4872',750000,25,29,432,4,'Với 40 lít thể tích giúp bạn có thể mang theo tất cả mọi thứ bạn cần cho chuyến đi của bạn. Với dây kéo bảng to và móc khó dễ kéo mở thuận tiện và thoải mái. Ngăn lưới bên trong đủ to để bỏ vừa hết vận dụng cá nhân. Hai ngăn phụ bên ngoài, 1 dây kéo,1 nút kim loại, để bỏ thêm vật dụng nhỏ cần thiết. Chất liệu G-1000 (65% Polyester 35% Cotton) Chống thấm nước. Kích thước 32 x 54 x 32 cm.','2018-06-09','/images/balo/b20.jpg'),(21,1,'The north face fuse box',530000,41,3,69,5,'Balo the north face fuse box base camp chính hãng được công ty the north face ra mắt vào năm 2018 với thiết kế nhỏ gọn và chịu lực cao phù hợp với những người có khu cầu di chuyển nhiều và nhu cầu mang nhiều vật dụng cá nhân có trọng lượng nhiều. Kích thước: 46 x 33 x 15cm. Chất liệu: TPE laminate (polyester 100%), 630 D nylon.','2018-06-28','/images/balo/b21.jpg'),(22,1,'The north face surge transit',790000,19,15,631,5,'Balo the north face surge transit chính hãng được công ty the north face ra mắt vào năm 2018 với thiết kế nhỏ gọn và chịu lực cao phù hợp với những người có khu cầu di chuyển nhiều và nhu cầu mang nhiều vật dụng cá nhân có trọng lượng nhiều. Chất liệu: TPE laminate (polyester 100%), 630 D nylon. Dung tích: 41L (2502in³).','2018-06-03','/images/balo/b22.jpg'),(23,1,'The north face hot shot',450000,25,19,327,5,'Balo the north face hot shot chính hãng được công ty the north face ra mắt vào năm 2018 với thiết kế nhỏ gọn và chịu lực cao phù hợp với những người có khu cầu di chuyển nhiều và nhu cầu mang nhiều vật dụng cá nhân có trọng lượng nhiều. Kích thước: 19.75″ x 13.75″ x 8.5″ (50 cm x 35 cm x 22 cm). Chất liệu: 500D Cordura® nylon matte ripstop (TNF Black, Dune Beige/Forest Night Green, Asphalt Grey/TNF Black, Forest Night Green/Acrylic Orange), 420D nylon, 1680D Ballistic s nylon.','2018-06-16','/images/balo/b23.jpg'),(24,1,'The north face surge new',650000,5,30,731,5,'Balo the north face surge new chính hãng được công ty the north face ra mắt vào năm 2018 với thiết kế nhỏ gọn và chịu lực cao phù hợp với những người có khu cầu di chuyển nhiều và nhu cầu mang nhiều vật dụng cá nhân có trọng lượng nhiều. Kích thước: 19.75” x 13.5” x 8.5” (50 cm x 34.5 cm x 22 cm). Chất liệu: 210D Cordura® nylon mini-ripstop (TNF Black, Spectra Yellow/TNF Black, Asphalt Grey/Krypton Green, Forest Night Green/Asphalt Grey), 420D nylon, 600D polyester print, 1680D Ballistic s nylon.','2018-05-18','/images/balo/b24.jpg'),(25,1,'The north face surge',650000,8,67,1605,5,'Balo the north face surge chính hãng được công ty the north face ra mắt vào năm 2018 với thiết kế nhỏ gọn và chịu lực cao phù hợp với những người có khu cầu di chuyển nhiều và nhu cầu mang nhiều vật dụng cá nhân có trọng lượng nhiều. Kích thước: 21.5” x 10”. Chất liệu: 500D Cordura® nylon matte ripstop.','2018-03-29','/images/balo/b25.jpg'),(26,1,'Sakos lamborghini i15',1140000,37,0,61,6,'Tên sản phẩm: Balo Neo Lamborghini i15. Màu: Đen, cam, đỏ, xám, nâu. Kích thước (RxSxC cm): 36 x 18 x 41. Chất liệu: Nylon, polyester, PE, EVA.','2018-06-28','/images/balo/b26.jpg'),(27,1,'Sakos discovery i15',885000,26,10,367,6,'Tên sản phẩm: Balo DISCOVERY I15. Màu sắc: DB (Xanh Đen), DG (Xám Đen), DN (Xanh Navy), RD (Đỏ). Kích thước (cm): 30x18x44. Chất liệu: Polyester, PE, PU.','2018-06-15','/images/balo/b27.jpg'),(28,1,'Sakos zen i15',930000,18,16,196,6,'Tên sản phẩm: Balo ZEN i15. Màu: Đen (BK), Nâu (BN), Xanh (DN). Chất liệu: Polyester, PE, PU.','2018-06-14','/images/balo/b28.jpg'),(29,1,'Sakos groovy 1',690000,39,1,63,6,'Tên sản phẩm: Balo GROOVY 1. Màu sắc: DB (xanh đen phối da màu nâu), DG (xám đen phối da màu đen), DN (xanh nay phối da màu đen), GN (xám xanh phối da màu nâu), RD (đỏ phối da màu đen). Kích thước (cm): 40 x 14 x 59. Chất liệu: Polyester, PU, PE, EVA, Synthetic leather.','2018-06-27','/images/balo/b29.jpg'),(30,1,'Sakos gruadian i15',930000,24,16,393,6,'Tên sản phẩm: Balo GUARDIAN i15. Màu: Đen (BK), Xanh (DN). Kích thước (cm): 30x18x44. Chất liệu: Polyester, PE, PU.','2018-06-10','/images/balo/b30.jpg'),(31,1,'Mikkor the royce backpack M',650000,29,5,831,7,'Mẫu The Royce Backpack được thiết kế trên nền vải 1000D Kodura chống nước tốt cùng vải lót Polyester 210D in logo thương hiệu nổi tạo nét riêng độc đáo của sản phẩm. Màu sắc: Đen. Kích thước: 44cm x 29cm x 16cm. ','2018-06-05','/images/balo/b31.jpg'),(32,1,'Mikkor the normad primier',590000,17,22,722,7,'MIKKOR tiếp tục đốt nóng với bộ sưu tập balo the normad primier thời trang, trẻ trung năng động dành cho học sinh, sinh viên và văn phòng. Kích thước: 44cm x 30cm x 16cm. Fit Laptop: 15.6”, for Mac 17”.','2018-06-03','/images/balo/b32.jpg'),(33,1,'Mikkor the arnold M navy',550000,23,13,531,7,'Tuyệt vời với thiết kế đẹp và sang trọng mang lại cho bạn cảm giác tuyệt vời khi sở hữu chiếc balô the arnold M navy này. Balô được thiết kế trên nền vải Kodura 1000D chóng nước cùng với đáy da PU mang nét đặc trưng thương hiệu Mikkor, vải lót bên trong được in nổi logo tạo nét riêng đọc đác của sản phẩm. Thiết kế với một ngăn chính lớn và 1 ngăn hộp dây kéo mặt trước túi. Ngăn chính gồm: 1 ngăn đựng đồ dùng lớn, 1 ngăn laptop kích thước 15.6\" PC, 1 ngăn đựng giấy tờ kích thước A4, 2 ngăn nhỏ đựng phụ kiện và 1 ngăn lưới lớn.','2018-06-17','/images/balo/b33.jpg'),(34,1,'Mikkor nomad backpack green',800000,20,6,611,7,'Mikkor - Nomad backpack green Phong cách thiết kế hiện đại, Mikkor giới thiệu đến tất cả các bạn dòng sản phẩm balô mới - The ARTHUR backpack Mikkor thiết kế dòng The Arthur Backpack trên nền vải chính N/1000D Kodura, PU80mg cao cấp, chóng nước tốt; vải lót trong toàn bộ P/210D, PU 40mg. Logo Mikkor chính được dập nổi bằng kim loại đính trên mặt thân trước balo.','2018-06-13','/images/balo/b34.jpg'),(35,1,'Mikkor ducer backpack',340000,7,13,198,7,'Mikkor - Ducer backpack Phong cách thiết kế hiện đại, Mikkor giới thiệu đến tất cả các bạn dòng sản phẩm balô mới - The ARTHUR backpack Mikkor thiết kế dòng The Arthur Backpack trên nền vải chính N/1000D Kodura, PU80mg cao cấp, chóng nước tốt; vải lót trong toàn bộ P/210D, PU 40mg. Logo Mikkor chính được dập nổi bằng kim loại đính trên mặt thân trước balo.','2018-06-18','/images/balo/b35.jpg'),(36,1,'Agva windor folded',970000,11,39,725,8,'Balo AGVA Windsor Folded 15.6” - Đen (LTB338) được làm bằng chất liệu vải Polyester trơn mịn, có khả năng hạn chế thấm nước rất tốt. Balo có tổng thể đơn giản với màu chủ đạo là màu xám khá thời thượng, thiết kế gọn nhẹ, đầu ngăn chính được bảo vệ bằng dây khóa kéo và gập lại cố định bởi nút từ tính. Ngăn chính của Balo AGVA Windsor Folded 15.6” - Đen (LTB338) được thiết kế ngăn riêng có thể đựng vừa laptop có kích thước tối đa lên đến 15.6 inch với lớp đệm bảo vệ, hạn chế trầy xước (balo có kích thước 33cm x 44cm x 43cm).','2018-05-25','/images/balo/b36.jpg'),(37,1,'Agva wanderer',1265000,30,10,642,8,'Balo AGVA Wanderer 15.6” được chú trọng về chất liệu sản xuất, nhằm đem đến sản phẩm thời trang, phù hợp nhu cầu của mọi người. Balo được làm từ chất liệu Polyester kết cấu Herringbone, hạn chế thấm nước tốt. Herringbone là kiểu vải được dệt chéo theo mô hình các đường zigzag đảo ngược hoặc như một bộ xương cá, có khả năng chịu nước cao hơn loại vải khác. Balo nam AGVA Wanderer 15.6” có kích thước (34 x 13 x 46 cm) rộng rãi, ngăn chứa tương thích dành cho laptop 15.6’’ trở xuống và một số ngăn phụ khác.','2018-06-19','/images/balo/b37.jpg'),(38,1,'Agva urban denim',1050000,29,5,182,8,'Balo AGVA Urban Denim 15.6” có 1 ngăn chính có không gian chứa đồ tương đối rộng rãi (kích thước balo là 27.5cm x 13cm x 45cm), trong đó có một ngăn chuyên đựng laptop có thể chứa laptop có kích thước tối đa 15.6 inch được trang bị thêm dây khóa giúp giữ cố định laptop. Đây đúng là một chiếc balo lý tưởng cho những bạn phải mang theo laptop để phục vụ cho công việc trong những chuyến du lịch ngắn ngày của mình. Được làm từ vải Polyester dệt theo kiểu Herringbone bển màu, có tuổi thọ sử dụng cao, có khả năng chống thấm nước tốt, giúp cho balo luôn giữ được vẻ ngoài sạch sẽ.','2018-06-16','/images/balo/b38.jpg'),(39,1,'Agva hailey',1050000,19,11,392,8,'Balo AGVA Hailey 13.3” - Đen (LTB342) được người dùng biết đến nhờ những đánh giá từ các chuyên gia trong lĩnh vực sản xuất balo đựng laptop. Đây là sản phẩm cao cấp vừa hợp thời trang vừa đạt tiêu chuẩn chất lượng đến từ nhãn hàng AGVA của đất nước Singapore. Chất liệu Polyester có khả năng hạn chế thấm nước rất tốt. Thiết kế riêng biệt dành cho laptop 13.3\".','2018-06-03','/images/balo/b39.jpg'),(40,1,'Agva heritage',1350000,15,12,462,8,'Balo được làm từ chất liệu vải Polyeste cao cấp cùng kết cấu Herribone chống thấm nước bảo vệ hiệu quả các vật dụng bên trong, nhờ vậy bạn có thể yên tâm hơn khi bất chợt gặp phải thời tiết mưa gió. Bạn có thể hoàn toàn yên tâm về việc bảo quản laptop của mình bởi ngăn đựng hết sức phù hợp cũng như êm ái. Thiết kế này giúp cho bạn luôn yên tâm khi mang theo laptop để đi học, làm việc,... Bên cạnh đó, kiểu dáng của sản phẩm cũng rất hiện đại, gọn nhẹ hỗ trợ bạn rất tốt trong việc mang theo bên mình ở bất cứ đâu.','2018-06-19','/images/balo/b40.jpg'),(49,2,'Adidas Convertible 3 Stripes Duffel Bag S Black',1150000,64,3,321,1,'Túi thể thao hằng ngày, hoặc dùng du lịch đến 3 - 4 ngày cực thoải mái! Chất liệu kháng nước hoàn toàn cho toàn bộ túi, có ngăn để giày/đồ ướt riêng chống mùi bên hông. Màu sắc và thiết kế basic dễ dùng cho cả nam và nữ!','2018-06-28','/images/bag/t1.jpg'),(50,2,'Nike Alpha Shoe Bag M Black',349000,31,9,161,4,'The Nike Alpha Adapt Shoe Bag allows for easy transport during travel. Its durable fabric makes for easy cleaning.','2018-06-27','/images/bag/t2.jpg'),(51,2,'Mountainsmith Vibe Waistpack S Black',500000,14,26,961,9,'Ultimate organization in a small lumbar pack, ready for a myriad of uses. Whether hiking, traveling or using it for everyday carry, you will be amazed at how much fits.','2018-06-19','/images/bag/t3.jpg'),(52,2,'AGVA Soho Slim 14 LTB337BLA M Black',990000,26,26,462,8,'AGVA Soho Slim 14 LTB337BLA M Black là dòng túi xách cao cấp, mang phong cách đơn giản, hiện đại của thương hiệu AGVA – Singapore. Túi xách thích hợp với thiết bị Laptop 14’’ trở xuống và dành cho nhiều đối tượng người sử dụng. Túi xách được làm từ chất liệu Nylon hạn chế thấm nước tốt. Kiểu dáng túi xách gọn nhẹ với 4 ngăn chứa đồ và hai kiểu quai hỗ trợ.','2018-06-03','/images/bag/t4.jpg'),(53,2,'AGVA Urban Denim 15 LTB260GRE M Grey',950000,19,4,261,8,'AGVA Urban Denim 15\\\" LTB260GRE M Grey là dòng túi xách cao cấp, mang phong cách hiện đại của thương hiệu AGVA – Singapore. Túi xách thích hợp với thiết bị Laptop 15’’ trở xuống và dành cho nhiều đối tượng người sử dụng. Túi xách được làm từ chất liệu Polyester hạn chế thấm nước tốt. Kiểu dáng túi xách gọn nhẹ với 3 ngăn chứa đồ và hai kiểu quai hỗ trợ.','2018-06-05','/images/bag/t5.jpg'),(54,2,'Seliux M5 Grant S Red',589000,37,13,642,10,'Seliux M5 Grant S Red là dòng sản phẩm túi đeo chéo cao cấp, thời trang phù hợp cho iPad Air. Túi có 4 màu sắc thời trang, thiết kế tiện lợi với dây quai to bản thay đổi chiều dài linh hoạt, nhiều ngăn dễ dàng sử dụng. Túi Seliux M5 Grant S được làm từ chất liệu 1000D Checken Tex SupremeTM bền màu, khả năng kháng nước tốt.','2018-06-24','/images/bag/t6.jpg'),(55,2,'Simplecarry Gymbag S Black',495000,54,4,101,11,'Sau những giờ làm việc căng thẳng, loanh quanh trong văn phòng công ty, chúng ta cần vận động, nâng cao sức khỏe. Khi bạn muốn có buổi luyện tập hiệu quả và chuyên nghiệp tại phòng tập, hãy mang theo túi thể thao Simplecarry Gymbag Grey/Black – một sản phẩm hoàn hảo được thiết kế riêng để giúp bạn đựng đồ thể thao khi đi tập.','2018-06-28','/images/bag/t7.jpg'),(56,2,'AGVA Heritage 15 LTB308GRE M Grey',850000,62,2,92,8,'AGVA Heritage 15 LTB308GRE M Grey là dòng túi xách cao cấp, mang phong cách hiện đại của thương hiệu AGVA – Singapore. Túi xách thích hợp với thiết bị Laptop 15’’ trở xuống và dành cho nhiều đối tượng người sử dụng. Túi xách được làm từ chất liệu Polyester hạn chế thấm nước tốt. Kiểu dáng túi xách gọn nhẹ với hai ngăn chứa đồ và hai kiểu quai hỗ trợ.','2018-06-27','/images/bag/t8.jpg'),(57,2,'Crumpler Proper Roady 2.0 Camera Sling 4500 S Blue',1725000,17,34,641,12,'Roady 2.0 Sling 4500 là túi máy ảnh hoàn hảo. Bạn có chỗ cho một máy ảnh DSLR với ống kính zoom, ống kính zoom tiêu chuẩn và một iPad 9.7\\\". Túi được xây dựng bằng một loại nylon không thấm nước và mềm mại, cung cấp tay cầm và ngăn đệm dành cho máy ảnh, iPad 9,7 inch, cũng như đệm lót lưng.','2018-06-11','/images/bag/t9.jpg'),(58,2,'Seliux G3 Widow Duffle M D.Grey',780000,62,1,29,10,'Seliux G3 Widow Duffle M D.Grey là dòng sản phẩm túi xách thể thao – du lịch của thương hiệu Seliux. Túi xách Seliux G3 Widow Duffle M thích hợp cho các bạn thường xuyên hoạt động thể thao hoặc đi du lịch/ công tác ngắn ngày chỉ cần đem theo ít đồ. Túi được làm từ chất liệu G1000 và lớp vải lót 210D Nylon hạn chế thấm nước tốt. Túi có kiểu dáng năng động, màu sắc hiện đại, đặc biệt có ngăn đựng giày được thiết kế riêng biệt.','2018-06-28','/images/bag/t10.jpg'),(59,2,'Seliux F10 Skynight Messenger M Navy',750000,42,5,61,10,'Seliux F10 Skynight Messenger M Navy là dòng túi xách Laptop thời trang, cao cấp của thương hiệu Seliux. Túi xách thích hợp cho Laptop 13’’ trở xuống, linh hoạt sử dụng trong các môi trường hoạt động khác nhau. Kiểu dáng năng động, màu sắc cá tính, làm từ chất liệu vải 1000D Chicken Tex, mặt đáy bằng Tapaulin hạn chế thấm nước rất tốt.','2018-06-27','/images/bag/t11.jpg'),(60,2,'Crumpler Muli Photo Sling 4500 M Black/Dark Navy',1790000,63,4,201,12,'Túi xách Crumpler Muli Photo Sling 4500 M Black/Dark Navy với thiết kế đơn giản mà không kém phần trẻ trung, cùng chất liệu vải cao cấp, đang được rất nhiều bạn trẻ yêu thích tin dùng. Với chiếc túi này, bạn có thể dễ dàng phối hợp cùng nhiều loại trang phục, thích hợp cho mọi hoàn cảnh như đi học, đi chơi hay đi dã ngoại, thể thao...','2018-06-22','/images/bag/t12.jpg'),(61,2,'Crumpler Quick Escape 400 S Black',550000,71,2,106,12,'Túi xách Crumpler Quick Escape 400 S Black là một túi vai linh hoạt với chỗ cho một chiếc máy ảnh chuyên nghiệp. Thiết kế thông minh, tiện dụng và chuyên nghiệp được kết hợp chất liệu an toàn. Đây là sản phẩm hàng đầu dành cho các bạn trẻ luôn yêu thích chụp hình.','2018-06-24','/images/bag/t13.jpg'),(62,2,'Seliux M1 Abrams S Grey/Black',390000,14,32,921,10,'Seliux là thương hiệu nổi tiếng, chuyên sản xuất các dòng vali, balo và túi xách cao cấp phục vụ nhu cầu người tiêu dùng. Túi đeo chéo Seliux M1 Abrams S Grey/Black kết hợp giữa tông màu đen ghi cùng điếm nhấn là logo màu đỏ nổi bật, sắc nét, gây ấn tượng cho người dùng từ cái nhìn đầu tiên.','2018-05-14','/images/bag/t14.jpg'),(63,2,'Herringbone Postman Small S Olive',2300000,35,14,156,13,'Túi máy ảnh Herringbone Postman Small S Olive là một dạng túi Messenger được thiết kế theo phong cách cổ điển nhưng vẫn giữ được nét tinh thế và sang trọng trong một kiểu dáng túi nhỏ gọn. Ở dòng túi này, bạn có thể để được khá nhiều vậy dụng đi kèm theo bộ máy.','2018-06-14','/images/bag/t15.jpg'),(64,2,'Crumpler Muli Messenger L Black Tarpaulin',1427000,25,25,254,12,'Thiết kế gồm 1 ngăn chính và 2 ngăn phụ. 1 Ngăn chính lớn cho phép đựng quần áo , sách vở với diện tích lớn. 1 ngăn máy tính xách tay 15\\\". 1 ngăn khóa kéo ở mặt trước. 1 ngăn khóa kéo ở mặt sau. Nệm êm và thoáng khí ở cả lưng và mặt ốp của dây đeo vai.','2018-06-19','/images/bag/t16.jpg'),(65,2,'Crumpler Proper Roady Photo Sling 7500 M Navy',1100000,33,23,279,12,'Túi xách Crumpler Proper Roady Photo Sling 7500 M Navy là dòng sản phẩm bán chạy hiện nay của thương hiệu Crumpler với thiết kế đơn giản mà không kém phần trẻ trung, cùng chất liệu vải cao cấp, đang được rất nhiều bạn trẻ yêu thích tin dùng. Với chiếc túi này, bạn có thể dễ dàng phối hợp cùng nhiều loại trang phục, thích hợp cho mọi hoàn cảnh như đi học, đi chơi hay đi dã ngoại, thể thao...','2018-06-06','/images/bag/t17.jpg'),(66,2,'Adidas Clima Team Bag S Orange',750000,61,9,93,1,'Adidas Clima Team Bag S Orange là dòng sản phẩm thích hợp dành cho các bạn thường xuyên tham gia các hoạt đông thể thao. Túi được thiết kế dây đeo chéo qua vai thời trang, bạn có thể đựng quần áo tập, giày, phụ kiện...','2018-06-25','/images/bag/t18.jpg'),(67,2,'Seliux F6 Skyray Duffel Bag M Grey',629000,24,26,921,10,'Túi xách Seliux F6 Skyray Duffel Bag M Grey là mẫu mới nhất và bán chạy nhất của dòng Seliux. Được thiết kế trên với những đường nét mềm mại với những nét kẻ sọc màu sắc đã tạo nên một tổng thể hoàn hảo, chiếc túi nổi bật hơn với tông màu sáng với họa tiết hoa văn thể hiện sự trẻ trung, năng động, gây ấn tượng cho người dùng.','2018-06-19','/images/bag/t19.jpg'),(68,2,'Puma Foundation Small Sports Bag S Pink/Navy',429000,62,3,38,14,'Túi thể thao Puma Foundation Small Sports Bag S Pink/Navy của thương hiệu PUMA được thiết kế tiện lợi, giúp bạn mang theo hành lý thuận tiện hơn trong mỗi chuyến du lịch hoặc hoạt động thể thao. Với thiết kế thông minh cùng chất liệu hoàn hảo, đây sẽ là sự lựa chọn tuyệt vời cho nhưng kỳ nghỉ ngắn ngày của gia đình bạn và tham gia vận động thể dục thể thao.','2018-06-22','/images/bag/t20.jpg'),(69,2,'Seliux Túi xách Ipad Exos 4004 M Brown',1798000,11,19,294,10,'Túi xách iPad Seliux Exos 4002 (M) Brown là mẫu mới nhất trong bộ sưu tập của dòng thương hiệu Seliux độc quyền. Thiết kế độc đáo, ấn tượng, thể hiện sự đơn giản cùng các điểm nhấn nổi bật rất trẻ trung và năng động.','2018-06-10','/images/bag/t21.jpg'),(70,2,'Seliux Túi xách laptop Exos 4003 M Brown',2578000,2,28,634,10,'Túi xách Seliux Exos 4003 (M) Brown là mẫu mới nhất trong bộ sưu tập của dòng thương hiệu Seliux độc quyền. Thiết kế độc đáo, ấn tượng, thể hiện sự đơn giản cùng các điểm nhấn nổi bật rất trẻ trung và năng động.','2018-06-01','/images/bag/t22.jpg'),(71,2,'Crumpler Betty Blue Sling M Brown',1894000,32,3,29,12,'Túi xách Crumpler Betty Blue Sling M Brown là một túi vai linh hoạt với chỗ cho một chiếc máy tính bảng mini hoặc lớn hơn của iPad. Nắp ngăn có một nắp clip nam châm giữ hàng ngày quan trọng của bạn nhanh chóng có thể truy cập. Giữ mọi thứ bằng cách cất an toàn điện thoại thông minh, chìa khóa hoặc máy nhắn tin cổ điển vào trong một túi nhỏ gọn bên trong.','2018-06-21','/images/bag/t23.jpg'),(72,2,'Adidas Originals Festival Sport S Black',550000,7,33,349,1,'Túi Đeo Chéo Adidas Originals Festival Sport là dòng đeo chéo của thương hiệu Adidas với thiết kế lạ mắt tạo nên phong cách độc đáo lạ mắt phù hợp với mọi lứa tuổi giới tính.','2018-05-17','/images/bag/t24.jpg'),(73,2,'Crumpler Shuttle delight laptop sleeve 13 M Gray',650000,28,22,297,12,'Crumpler Shuttle delight laptop sleeve 13\\\" M Gray là dòng sản phẩm túi chống sốc của thương hiệu Crumpler dành cho các sản phẩm laptop size 13’’, Macbook Pro, iPad Mini và các phụ kiện kèm theo khác. Màu sắc nhã nhặn, được làm từ chất liệu vải Polyester 600D kháng nước tốt, máy tính chất lượng túi và máy tính của bạn đều được bảo vệ tối đa.','2018-04-30','/images/bag/t25.jpg'),(74,2,'Adidas NMD Crossbody Bag CE2377 S Black',1099000,3,27,266,1,'Clean and contemporary, this crossbody bag has the sleek, technical look and feel that defines NMD sportswear. It has a top zip compartment with an additional zip pocket inside. The padded shoulder strap adjusts to the perfect length and has a rubber buckle closure.','2018-05-19','/images/bag/t26.jpg'),(75,2,'SRSLY Singapore Plus 12inch S Gray',675000,6,54,1602,15,'SRSLY Singapore Plus 12inch S Gray là dòng sản phẩm túi chống sốc của thương hiệu SRSLY – Đan Mạch. Túi SRSLY Singapore Plus 12inch S Gray sở hữu thiết kế mang đậm phong cách Bắc Âu lịch lãm thích hợp cho thiết bị Laptop 12\'\'. Túi SRSLY Singapore Plus nổi bật với chất liệu mút cao su EVA kết hợp vải Polyester phủ bên ngoài hạn chế thấm nước, nhẹ và bền, tránh va đập cho thiết bị.','2018-05-29','/images/bag/t27.jpg'),(76,2,'SRSLY Atlanta 30cm S Gray',675000,1,29,432,15,'SRSLY Atlanta 30cm S Gray là dòng sản phẩm túi trống thể thao – du lịch của thương hiệu SRSLY – Đan Mạch. Túi SRSLY Atlanta 30cm S Gray sở hữu thiết kế mang đậm phong cách Bắc Âu lịch lãm. Túi SRSLY Atlanta nổi bật với chất liệu mút cao su EVA kết hợp vải Polyester phủ bên ngoài hạn chế thấm nước, nhẹ và bền.','2018-06-19','/images/bag/t28.jpg'),(77,2,'Kakashi Raiden Túi Đựng Giày S Moss',300000,51,9,64,16,'Kakashi Raiden Bag S Moss là dòng sản phẩm phụ kiện túi đựng dày, thời trang, tiện lợi của thương hiệu Kakashi. Túi thích hợp dành cho các bạn thường xuyên tham gia hoạt động thể thao hoặc cần đựng giày để sắp xếp vào vali trong các chuyến đi. Túi đựng giày Kakashi Raiden Bag S Moss được làm từ chất liệu Nylon hạn chế thấm nước rất tốt.','2018-06-23','/images/bag/t29.jpg'),(78,2,'Rivacase 8221 M Blue',690000,25,15,621,17,'Rivacase 8221 M Blue là dòng sản phẩm túi xách Laptop thời trang, cao cấp của thương hiệu Rivacase – Đức. Túi xách có thiết kế thích hợp cho Laptop 13.3\\\", cộng thêm nhiều ngăn phụ hỗ trợ sắp xếp đồ đạc. Túi xách Rivacase 8221 được làm từ chất liệu Polyester kết hợp vải lót Nylon 150D bên trong ngăn hạn chế thấm nước tốt, bền màu với thời gian.','2018-06-09','/images/bag/t30.jpg'),(79,2,'Tucano Contatto Large-CBC-L-G M Grey',1280000,36,24,324,18,'Tucano Contatto Large-CBC-L-G M Grey là dòng túi máy ảnh đeo vai nhỏ gọn với thiết kế trang nhã, chất liệu không thấm nước. Contatto với nhiều cải tiến trong thiết kế sẽ là một mẫu túi đeo vai giúp bảo vệ lý tưởng cho máy ảnh của bạn.','2018-06-14','/images/bag/t31.jpg'),(80,2,'Tucano Work Out 3 Pop-Up Bag 15 WO3U-MB15-B M Blue',2267000,24,36,292,18,'Business bag per MacBook 15” o ultrabook 15.6” con vano interno espandibile che da borsa media la trasforma in borsa grande. La tasca, in contrasto colore, accessorio versatile di tutta la collezione Work_Out 3, può essere usata in tre modi diversi: totalmente nascosta, parzialmente visibile o estratta completamente, per una estensione di capienza. In dotazione tracolla removibile e vano interno Anti-shock System per la protezione del device.','2018-06-12','/images/bag/t32.jpg'),(81,2,'Case Logic SLR Camera Holster SLRC 200 S Black',700000,85,5,29,19,'Túi máy ảnh Case Logic SLR Camera Holster với kiểu dáng nhỏ gọn và đơn giản. Thiết kế thuận tiện để lấy máy ảnh nhanh chóng. Phù hợp cho các máy nhỏ gọn như nikon d90, d7000, canon 500d, 600d..v..v. Dưới đáy có miếng nhựa giúp để xuống nền đất hay nơi nào đó bị ẩm ướt vẫn yên tâm.','2018-06-26','/images/bag/t33.jpg'),(82,2,'Kakashi Katou Duffle Bag M Navy',715000,24,26,612,16,'Kakashi Katou Duffle Bag M Navy là dòng sản phẩm túi trống thích hợp dành cho các bạn thường xuyên tham gia các hoạt đông thể thao. Túi được thiết kế dây đeo qua vai thời trang, bạn có thể sắp xếp đồ đạc trong ngăn chính và những ngăn phụ khác. Túi Kakashi Katou Duffle được làm từ chất liệu 100% Polyester kết hợp vải lót 420D Ripstop hạn chế thấm nước tốt.','2018-06-10','/images/bag/t34.jpg'),(83,2,'Tucano Stria Macbook 15 BSTR15-BK M Black',1280000,21,29,258,18,'Tucano Stria Macbook 15\\\" BSTR15-BK M Black là dòng sản phẩm túi xách laptop cao cấp của thương hiệu Tucano đến từ nước Ý. Túi xách là sự kết hợp giữa hai chất liệu vải Polyester bền, chống thấm nước cực tốt. Tucano Stria Macbook 15’’ mang đến cho bạn một chiếc túi vừa thời trang, vừa đảm bảo chức năng chống sốc, bảo vệ an toàn cho các thiết bị.','2018-06-08','/images/bag/t35.jpg'),(84,2,'Tucano Modo Business Macbook 15 BMDOB-BK M Black',1790000,31,19,612,18,'Tucano Modo Business Macbook 15 BMDOB-BK M Black là dòng sản phẩm túi xách laptop cao cấp của thương hiệu Tucano đến từ nước Ý. Túi xách là sự kết hợp giữa hai chất liệu vải Polyester chống thấm nước cực tốt. Tucano Modo Business Macbook 15’’ mang đến cho bạn một chiếc túi vừa thời trang, vừa đảm bảo chức năng chống sốc, bảo vệ an toàn cho các thiết bị.','2018-06-14','/images/bag/t36.jpg'),(85,2,'Tucano Piu Macbook 15 BPB15-BK M Black',1120000,25,35,799,18,'Tucano Piu Macbook 15\\\" BPB15-BK M Black là dòng sản phẩm túi xách Macbook, Laptop cao cấp của thương hiệu Tucano đến từ nước Ý. Túi xách là sự kết hợp giữa hai chất liệu vải Nylon bền, chống thấm nước cực tốt. Tucano Piu Macbook 15’’ mang đến cho bạn một chiếc túi vừa thời trang, vừa đảm bảo chức năng chống sốc, bảo vệ an toàn cho các thiết bị.','2018-05-19','/images/bag/t37.jpg'),(86,2,'Kakashi Miro Túi Đựng Mỹ Phẩm Du Lịch S Navy',250000,17,23,615,16,'Khi sở hữu quá nhiều loại mỹ phẩm cùng dụng cụ trang điểm thì việc bảo quản để tất cả luôn được gọn gàng, ngăn nắp trở thành vấn đề nan giải với nhiều người. Hiểu được những lo lắng đó, Kakashi mang đến cho bạn sản phẩm túi đựng mỹ phẩm Kakashi Miro Bag S Navy với thiết kế xinh xắn, để bạn yên tâm hơn khi cất giữ mỹ phẩm và các vật dụng cần thiết khác.','2018-06-19','/images/bag/t38.jpg'),(87,2,'Kakashi Tenjin Túi Đựng Mỹ Phẩm S Red',420000,26,4,47,16,'Bạn đang tìm một chiếc túi đựng mỹ phẩm du lịch không chỉ đẹp đẹp, năng động, mà còn có khả năng chống thấm nước để đựng và bảo vệ cho những vật dụng cá nhân, mỹ phẩm cần thiết cho chuyến đi, túi đựng mỹ phẩm Kakashi Tenjin bag S Red sẽ là sự lựa chọn hoàn hảo cho bạn.','2018-06-27','/images/bag/t39.jpg'),(88,2,'Tucano CB_TP_S S Black',490000,26,14,385,18,'Túi xách có thể xếp gọn Tucano với kích thước vửa phải phù hợp cả nam lẫn nữ, cho phép bạn cùng lúc mang theo máy tính xách tay và nhiều loại giấy tờ khác, để thực hiện một chuyến đi trong ngày. Túi xách Tucano CB_TP_S S Black thiết kế nhỏ gọn cùng chất liệu chống thấm nước, đây là sự lựa chọn mới cho bạn.','2018-06-19','/images/bag/t40.jpg');
/*!40000 ALTER TABLE `sanpham` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) unsigned NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('Ee4WiBHszck21AXV-kpeLZ_TF9BybEzW',1530269771,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":true,\"curKH\":10}'),('KKmD3fNqVf2I47ektcjk_PIaKN7FQe2n',1530301428,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":true,\"curKH\":10,\"cartPro\":0,\"isAdmin\":false,\"diachi\":\"Đường 25, q7\",\"sodienthoai\":\"1234\",\"ten\":\"Phat Truong\"}'),('ZzTGULPQmdqB2-ElHPNVJSf-ExCxM2pj',1530268559,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":true,\"curKH\":10}'),('_qDNV5a0J2p73BKZkqbrBWGDI7r0MmNS',1530326362,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":false,\"isAdmin\":false,\"cartPro\":0}'),('sh06j4cpUZguZ66SZMpZsiuYxwdkWUeU',1530339414,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":true,\"isAdmin\":false,\"cartPro\":0,\"curKH\":10,\"diachi\":\"Đường 25, q7\",\"sodienthoai\":\"1234\",\"ten\":\"Phat Truong\"}'),('vmE0PlfudvCT8_lEIxhz4lV2_Bo_xpPb',1530326384,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":false,\"isAdmin\":false,\"cartPro\":0}'),('wPOJpkYT54RSXGLgd6VsN0MeXV7y9jeY',1530327764,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"isLogged\":true,\"isAdmin\":false,\"cartPro\":0,\"curKH\":10,\"diachi\":\"Đường 25, q7\",\"sodienthoai\":\"1234\",\"ten\":\"Phat Truong\"}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-01  9:23:26
