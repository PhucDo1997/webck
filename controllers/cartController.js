var express = require('express');
var cartRepo = require('../repos/cartRepo'),
    productRepo = require('../repos/productRepo');
billRepo = require('../repos/billRepo');

var router = express.Router();

router.get('/', (req, res) => {
    var id = req.session.curKH;
    cartRepo.loadGioHang(id).then(result => {
        var vm;
        console.log(result.length);
        if (result.length > 0) {
            var amount = 0;
            for (var i = 0; i < result.length; i++) {
                amount += result[i].gia * result[i].soluong;
            }
            vm = {
                cartNum: req.session.cartPro,
                items: result,
                isNoProduct: false,
                ten: req.session.ten,
                diachi: req.session.diachi,
                sodienthoai: req.session.sodienthoai,
                Amount: amount
            };
            console.log(vm);
            res.render('cart/index', vm);
        }
        else {
            vm = {
                cartNum: req.session.cartPro,
                isNoProduct: true,
                Amount: 0
            }
            console.log('render ko san pham');
            res.render('cart/index', vm);
        }
    }).catch(error => { console.log('caught', error.message); });
});

router.post('/add', async (req, res) => {
    var proid = req.body.masp;
    var userid = req.session.curKH;
    var cartitem = await cartRepo.loadSingle(userid, proid);
    if (cartitem.length > 0) {
        var vm = {
            makh: userid,
            masp: proid,
            soluong: cartitem[0].soluong + 1
        }
        var update = await cartRepo.updateSoLuong(vm);
    }
    else {
        var vm = {
            makh: userid,
            masp: proid,
            soluong: 1
        }
        var update = await cartRepo.add(vm);
    }
    res.redirect('/cart');
});

router.post('/remove', async (req, res) => {
    console.log("Đã vào remove");
    var a = await cartRepo.remove(req.session.curKH, req.body.proID);
    res.redirect(req.headers.referer);
});

router.post('/increase', async (req, res) => {
    console.log("tăng số lượng sản phẩm");
    var b = await cartRepo.loadSingle(req.session.curKH, req.body.proID_I);
    var c = await productRepo.single(req.body.proID_I);
    if (b[0].soluong < c[0].soluongcon) {
        console.log(b[0].soluong);
        var cartitem = {
            makh: req.session.curKH,
            masp: req.body.proID_I,
            soluong: b[0].soluong + 1
        }
        console.log(cartitem.soluong);
        var a = await cartRepo.updateSoLuong(cartitem);
    }
    res.redirect(req.headers.referer);
});

router.post('/decrease', async (req, res) => {
    var b = await cartRepo.loadSingle(req.session.curKH, req.body.proID_D);
    if (b[0].soluong > 1) {
        console.log(b[0].soluong);
        var cartitem = {
            makh: req.session.curKH,
            masp: req.body.proID_D,
            soluong: b[0].soluong - 1
        }
        var a = await cartRepo.updateSoLuong(cartitem);
    }
    res.redirect(req.headers.referer);
});

router.post('/thanhtoan', (req, res) => {
    //Tạo đơn
    var t = new Date();
    var dd = t.getDate();
    var mm = t.getMonth() + 1; //January is 0!
    var red = res;
    var yyyy = t.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '/' + mm + '/' + dd;
    var billItem = {
        makh: req.session.curKH,
        tennguoinhan: req.body.tennguoinhan,
        diachinhan: req.body.diachinhan,
        sodienthoai: req.body.sodienthoai,
        ghichu: req.body.ghichu,
        ngaymua: today
    }
    billRepo.insertBill(billItem).then(resultInsBill => {
        //Tạo chi tiết đơn
        //console.log('result Billllllllll');
        var a = resultInsBill;
        //var detailBillItem;
        cartRepo.loadGioHang(req.session.curKH).then(result => {
            console.log(".......");
            console.log(result);
            var products = result;
            billRepo.insertDetailBill({sanpham: result, madon: a.insertId}).then(res => {
                //Xóa giỏ hàng
                console.log(res);
                console.log('Xoa gio hang');    
                console.log(req.session.curKH);
                cartRepo.removeAll(req.session.curKH).then(() => {
                    red.redirect(req.headers.referer);
                });
            });
            
            for (var i = 0; i < products.length; i++) {
                
                productRepo.updateSoLuongCon(products[i].makh, products[i].soluongcon);
                
            }
            //Cập nhật số lượng còn
            console.log(billItem);
            
        });
        //alert("Bạn đã đặt hàng thành công!");
    });
});

module.exports = router;