var express = require('express');
var productRepo = require('../repos/productRepo');
var config = require('../config/config');

var router = express.Router();

// router.get('/byCat', (req, res) => {
//     var catId = req.query.catId;
//     productRepo.loadAllByCat(catId).then(rows => {
//         var vm = {
//             products: rows
//         };
//         res.render('product/byCat', vm);
//     });
// });

// router.get('/byCat/:catId', (req, res) => {
//     var catId = req.params.catId;
//     productRepo.loadAllByCat(catId).then(rows => {
//         var vm = {
//             products: rows,
//             noProducts: rows.length === 0
//         };
//         res.render('product/byCat', vm);
//     });
// });

router.get('/byCat/:catId/:brandId', (req, res) => {
    var catId = req.params.catId;
    var brandId = req.params.brandId;
    var page = req.query.page;
    if (!page) {
        page = 1;
    }
    var offset = (page - 1) * config.PRODUCTS_PER_PAGE;
    var p1;
    var p2;
    if(brandId == "noBrand"){
        p1 = productRepo.loadAllByCat(catId, offset);
        p2 = productRepo.countByCat(catId);
    }else{
        p1 = productRepo.loadAllByCatBrand(catId, offset, brandId);
        p2 = productRepo.countByCatBrand(catId, brandId);
    }
    
    var p3 = productRepo.loadBestSellerProduct();
    var p4 = productRepo.loadBrand();
    Promise.all([p1, p2, p3, p4]).then(([pRows, countRows, bestSellerRow, brand]) => {
        
        var total = countRows[0].total;
        var nPages = total / config.PRODUCTS_PER_PAGE;
        if (total % config.PRODUCTS_PER_PAGE > 0) {
            nPages++;
        }
        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }

        var pageName;
    if(catId =='bag') pageName = "Túi xách";
    if(catId =='balo') pageName = "Ba lô";

        var vm = {
            cartNum: req.session.cartPro,
            products: pRows,
            noProducts: pRows.length === 0,
            page_numbers: numbers,
            bestSeller : bestSellerRow,
            pageName: pageName,
            brand: brand
        };
        for (var row in vm.brand){
            vm.brand[row].catId = catId;
        };
        res.render('product/byCat', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

router.get('/detail/:proId', (req, res) => {
    var proId = req.params.proId;
    console.log(proId);
    var product = productRepo.single(proId);
    var sameBrand = productRepo.loadSameBrand(proId);
    var sameType = productRepo.loadSameType(proId);
    Promise.all([product, sameBrand, sameType]).then(([product, brand, type]) =>{
        var vm = {
            cartNum: req.session.cartPro,
            product: product[0],
            sameBrand: brand,
            sameType: type
        }
        res.render('product/detail', vm);
        console.log(sameBrand);
        console.log(sameType);
    });
});



router.post('/search', (req, res) => {
    var type = req.body.type;
    console.log("type: "+type);
    var search = req.body.search;
    console.log(search);
    var page = req.body.page;
    
    if (page == 0) {
        page = 1;
    }else{
        page = +page;
    }
    var offset = (page - 1) * config.PRODUCTS_PER_PAGE;

    var p1 = productRepo.search(type, search, offset);
    var p2 = productRepo.countBySearch(type, search);
    Promise.all([p1, p2]).then(([pRows, countRows]) => {
        var total = countRows[0].total;
        var nPages = total / config.PRODUCTS_PER_PAGE;
        if (total % config.PRODUCTS_PER_PAGE > 0) {
            nPages++;
        }
        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }

        var vm = {
            products: pRows,
            noProducts: pRows.length === 0,
            page_numbers: numbers,
            type: type,
            search: search
        };
        res.render('product/search', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

module.exports = router;