var express = require('express');
var adminRepo = require('../repos/adminRepo');
var productRepo = require('../repos/productRepo');
var router = express.Router();

var moment = require('moment');

router.get('/', (req, res) => {
    // var newProduct = productRepo.loadNewProduct();
    // var commonProduct = productRepo.loadCommonProduct();
    // var bestSellerProduct = productRepo.loadBestSellerProduct();
    
    // Promise.all([newProduct,commonProduct, bestSellerProduct]).then(([pRow1, pRow2, pRow3])=>{
        
    //     var vm = {
    //         newProduct: pRow1,
    //         commonProduct: pRow2,
    //         bestSellerProduct: pRow3
    //     };
    //     //console.log(vm.NP);
    //     res.render('home/index', vm);
    // });
    res.redirect('/admin/dashboard');
});

router.get('/dashboard', (req, res) => {
    var toDaySale = adminRepo.getToDaySale();
    var toDayBill = adminRepo.getToDayBill();
    var totalSale = adminRepo.getTotalSale();
    var totalBill = adminRepo.getTotalBill();

    Promise.all([toDaySale, toDayBill, totalSale, totalBill]).then(([pRow1, pRow2, pRow3, pRow4])=>{

        var vm = {
            toDaySale: pRow1[0].total,
            toDayBill: pRow2[0].total,
            totalSale: pRow3[0].total,
            totalBill: pRow4[0].total
        };
        res.render('admin/dashboard', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

router.get('/bill', (req, res) => {
    
    var page = req.query.page;
    if (!page) {
        page = 1;
    }

    var offset = (page - 1) * 5;

    var bill = adminRepo.loadAllBill(offset);
    var p = adminRepo.countAllBill();
    
    Promise.all([bill, p]).then(([pRows, countRows])=>{
        var total = countRows[0].total;
        var nPages = total / 5;
        if (total % 5 > 0) {
            nPages++;
        }
        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }
        for (var row in pRows){
            pRows[row]['ngaymua'] = moment(pRows[row]['ngaymua']).format('DD/MM/YYYY');
            switch(pRows[row]['tinhtrang']){
                case "0":
                    pRows[row]['tinhtrang'] = "Chưa giao";
                    pRows[row]['color'] = "warning";
                    break;
                case "1":
                    pRows[row]['tinhtrang'] = "Đang giao";
                    pRows[row]['color'] = "info";
                    break;
                case "2":
                    pRows[row]['tinhtrang'] = "Đã giao";
                    pRows[row]['color'] = "success";
                    break;
            };
        }
        var vm = {
            bill: pRows,
            noBill: pRows.length === 0,
            page_numbers: numbers
        };
        res.render('admin/billManager', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

router.get('/detail/:billId', (req, res) => {
    var BillId = req.params.billId;
    
    var bill = adminRepo.loadSingleBill(BillId);
    var c = adminRepo.loadCustomerBill(BillId);
    var p = adminRepo.loadProductBill(BillId);
    Promise.all([bill, c, p]).then(([pRows, customer, products])=>{
        
        for (var row in pRows){
            pRows[row]['ngaymua'] = moment(pRows[row]['ngaymua']).format('DD/MM/YYYY');
            switch(pRows[row]['tinhtrang']){
                case "0":
                    pRows[row]['tinhtrang'] = "Chưa giao";
                    break;
                case "1":
                    pRows[row]['tinhtrang'] = "Đang giao";
                    break;
                case "2":
                    pRows[row]['tinhtrang'] = "Đã giao";
                    break;
            };
        }
        
        var sum = 0;
        for (var row in products){
            sum += products[row]['sotien'];
        }

        var vm = {
            bill: pRows[0],
            customer: customer[0],
            product: products
        };
        vm.customer.tongtien = sum;
        
        res.render('admin/detail', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

router.get('/category', (req, res) => {
    var slb1 = adminRepo.soLuongBanByCat(1);
    var slb2 = adminRepo.soLuongBanByCat(2);
    var brands = productRepo.loadBrand();
    Promise.all([slb1, slb2, brands]).then(([pRow1, pRow2, pRow3])=>{
        var vm = {
            bagSale: pRow1[0].total,
            baloSale: pRow2[0] .total,
            brand: pRow3
        };
        
        res.render('admin/categoryManager', vm);
    }).catch(error=>{console.log('caught', error.message);});
    
});

router.post('/bill/change', (req, res) => {
    adminRepo.changeStatus(req.body.billId, req.body.status).then(value => {
        res.redirect('/admin/bill');
    });
});
module.exports = router;