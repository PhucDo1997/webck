var express = require('express'),
    SHA256 = require('crypto-js/sha256'),
    moment = require('moment');

var accountRepo = require('../repos/accountRepo');
var restrict = require('../middle-wares/restrict');
var cartContrl = require('./cartController')

var router = express.Router();

router.get('/register', (req, res) => {
    res.render('account/register');
});

router.get('/login',(req, res)=>{
    res.render('account/login');
});

router.post('/register', (req, res) => {
    console.log(req.body.sdt);
    accountRepo.isExistAccount(req.body.sdt).then(value =>{
        var info;
        if(value.length == 0){
            info = {
                ten: req.body.name,
                matkhau: SHA256(req.body.rawPWD).toString(),
                gioitinh: req.body.gt,
                diachi: req.body.addr,
                sdt: req.body.sdt,
                email: req.body.email
            }
            console.log(info);
            accountRepo.add(info).then(value =>{
                //Tra ve thanh cong hay khong
                //Tra ve thong tin dien lai form
                if(value.affectedRows == 1){
                    res.render('account/login', info);
                }
                else{
                    res.render('account/register', info);
                }
            });
        }
    });
});

router.post('/login', (req, res)=>{
    var sdt = req.body.username;
    var mk =req.body.rawPWD;

    accountRepo.checkLoginUser(sdt, mk).then(value=>{
        if(value.length == 1){
            console.log(value);
            console.log(value[0].matkhau);
            req.session.curKH = value[0].makh;
            req.session.diachi = value[0].diachi;
            req.session.sodienthoai = value[0].sodienthoai;
            req.session.ten = value[0].ten;
            req.session.isLogged = true;
           
            res.redirect('/');
        }

        else{
            accountRepo.checkLoginAdmin(sdt, mk).then(value=>{
                if(value.length == 1){
                    req.session.isAdmin = true;
                    req.session.isLogged = true;
                    res.redirect('/admin');
                }
                else{
                    var vm = {
                        cartNum: req.session.cartPro,
                        err:true
                    }
                    res.render('account/login', vm);
                }
            });
        }

    });

});

router.get('/profile', restrict, (req, res) => {
    accountRepo.getUser(req.session.curKH).then(value=>{
        var vm = {
            user: value[0]
        };
        if(vm.user.gioitinh == 1){
            vm.user.gender = "Nam";
        }else{
            vm.user.gender = "Nữ";
        }
        res.render('account/profile', vm);
    });
});

router.post('/logout', (req, res) => {
    req.session.isLogged = false;
    req.session.curKH = null;
    req.session.isAdmin = false;
    // req.session.cart = [];
    res.redirect(req.headers.referer);
});

router.post('/update', (req, res) => {
    var makh = req.session.curKH;
    var user = {
        makh: makh,
        ten: req.body.ten,
        gioitinh: req.body.gioitinh,
        diachi: req.body.diachi,
        sodienthoai: req.body.sodienthoai,
        email: req.body.email
    };
    accountRepo.update(user).then(value=>{
        var errorMsg;
        var color;
        if(value.changedRows != 0){
            errorMsg = "Update thông tin thành công!";
            color = "success";
        }else{
            errorMsg = "Update không thành công!";
            color = "danger";
        }
        accountRepo.getUser(req.session.curKH).then(value=>{
            var vm = {
                user: value[0],
                err: true,
                errorMsg: errorMsg,
                color: color
            };
            if(vm.user.gioitinh == 1){
                vm.user.gender = "Nam";
            }else{
                vm.user.gender = "Nữ";
            }
            res.render('account/profile', vm);
        });
    });
    
});

module.exports = router;