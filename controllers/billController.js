var express = require('express');
var billRepo = require('../repos/billRepo');
var router = express.Router();

var moment = require('moment');

router.get('/', (req, res) => {
    
    res.redirect('/bill/history');
});


router.get('/history', (req, res) => {
    var maKH = req.session.curKH;
    var page = req.query.page;
    if (!page) {
        page = 1;
    }

    var offset = (page - 1) * 5;

    var bill = billRepo.loadAllBill(offset, maKH);
    var p = billRepo.countAllBill(maKH);
    
    Promise.all([bill, p]).then(([pRows, countRows])=>{
        var total = countRows[0].total;
        var nPages = total / 5;
        if (total % 5 > 0) {
            nPages++;
        }
        var numbers = [];
        for (i = 1; i <= nPages; i++) {
            numbers.push({
                value: i,
                isCurPage: i === +page
            });
        }
        for (var row in pRows){
            pRows[row]['ngaymua'] = moment(pRows[row]['ngaymua']).format('DD/MM/YYYY');
            switch(pRows[row]['tinhtrang']){
                case "0":
                    pRows[row]['tinhtrang'] = "Chưa giao";
                    pRows[row]['color'] = "warning";
                    break;
                case "1":
                    pRows[row]['tinhtrang'] = "Đang giao";
                    pRows[row]['color'] = "info";
                    break;
                case "2":
                    pRows[row]['tinhtrang'] = "Đã giao";
                    pRows[row]['color'] = "success";
                    break;
            };
        }
        var vm = {
            bill: pRows,
            noBill: pRows.length === 0,
            page_numbers: numbers
        };
        res.render('bill/historyBill', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

router.get('/detail/:billId', (req, res) => {
    var BillId = req.params.billId;
    
    var bill = billRepo.loadSingleBill(BillId);
    var c = billRepo.loadCustomerBill(BillId);
    var p = billRepo.loadProductBill(BillId);
    Promise.all([bill, c, p]).then(([pRows, customer, products])=>{
        
        for (var row in pRows){
            pRows[row]['ngaymua'] = moment(pRows[row]['ngaymua']).format('DD/MM/YYYY');
            switch(pRows[row]['tinhtrang']){
                case "0":
                    pRows[row]['tinhtrang'] = "Chưa giao";
                    break;
                case "1":
                    pRows[row]['tinhtrang'] = "Đang giao";
                    break;
                case "2":
                    pRows[row]['tinhtrang'] = "Đã giao";
                    break;
            };
        }
        
        var sum = 0;
        for (var row in products){
            sum += products[row]['sotien'];
        }

        var vm = {
            bill: pRows[0],
            customer: customer[0],
            product: products
        };
        vm.customer.tongtien = sum;
        
        res.render('bill/detailBill', vm);
    }).catch(error=>{console.log('caught', error.message);});
});

module.exports = router;