var express = require('express');
var cartRepo = require('../repos/cartRepo');
var productRepo = require('../repos/productRepo');
var router = express.Router();

router.get('/', async (req, res) => {
    var c = 0;
    if(req.session.isLogged == true){
        c = await cartRepo.TongSoLuong(req.session.curKH);
    }
    //var d = await cartRepo.TongSoLuong(req.session.curKH);
    var newProduct = productRepo.loadNewProduct();//mới
    var commonProduct = productRepo.loadCommonProduct();//xem nhiều
    var sellerProduct = productRepo.loadSellerProduct();//bán chạy
    
    Promise.all([newProduct,commonProduct, sellerProduct, c]).then(([pRow1, pRow2, pRow3, cartNumber])=>{
        req.session.cartPro = cartNumber;
        console.log(cartNumber);
        var vm = {
            cartNum: req.session.cartPro,
            newProduct: pRow1,
            commonProduct: pRow2,
            sellerProduct: pRow3
        };
        console.log(vm.cartNum);
        res.render('home/index', vm);
    });
    
});

router.get('/about', (req, res) => {
    res.render('home/about');
});

module.exports = router;