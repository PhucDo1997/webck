jQuery(document).ready(function ($) {
	$('#same-products').owlCarousel({
		loop: true,
		center: true,
		items: 5,
		margin: 30,
		autoplay: true,
		dots: true,
		nav: true,
		autoplayTimeout: 8500,
		smartSpeed: 450,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1170: {
				items: 5
			}
		}
	});

	$('#same-brands').owlCarousel({
		loop: true,
		center: true,
		items: 5,
		margin: 30,
		autoplay: true,
		dots: true,
		nav: true,
		autoplayTimeout: 8500,
		smartSpeed: 450,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1170: {
				items: 5
			}
		}
	});

	$('#img-item-info').owlCarousel({
		loop: true,
		center: true,
		items: 5,
		margin: 30,
		autoplay: true,
		dots: true,
		nav: true,
		autoplayTimeout: 8500,
		smartSpeed: 450,
		navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		responsive: {
			0: {
				items: 1
			},
			768: {
				items: 1
			},
			1170: {
				items: 1
			}
		}
	});

	$('.main-menu li:nth-child(3)').on('click', function (event) {
		event.preventDefault();
		$("html,body").animate({ scrollTop: $('.brand-products').offset().top - 50 }, "slow");
	});

	$('#info-product-detail').on('click', function () {
		event.preventDefault();
		$('.info-product-detail').removeClass('hide');
		$('.detail-number').addClass('hide');
	});

	$('#number-tech').on('click', function () {
		event.preventDefault();
		$('.detail-number').removeClass('hide');
		$('.info-product-detail').addClass('hide');
	});

	$('.toTop').on('click', function (event) {
		event.preventDefault();
		$("html,body").animate({ scrollTop: 0 }, "slow");
	});
});