# Đồ án cuối kỳ môn phát triển ứng dụng web

## Thông tin các thành viên trong nhóm

-   MSSV  -       Tên       -   Tên commit
- 1512402 - Đỗ hồng phúc    - Do Hong Phuc
- 1512396 - Trương Tấn Phát - Truong Tan Phat
- 1512456 - Bùi Việt Sơn    - kiencon

### Cài đặt trước khi chạy chương trình

- Clone project về máy
- Cần cài đặt cơ sở dữ liệu Mysql với link nguồn trong project như sau

```
database/quanlysanpham.sql
```

- sửa lại username, password và port của Mysql để kết nối với cơ sở dữ liệu ở các file sau

```
app.js
fn/db.js
```

- Các package npm đã được cài đặt sẵn và push lên source trong project

### chạy chương trình

nhập lệnh sau trên cmd ở folder project

```
npm start
```

Sau đó lên trình duyệt web truy cập đến địa chỉ sau để vào trang web

```
localhost:3000
```

## Được viết bởi ngôn ngữ

* [Nodejs](https://nodejs.org/en/)