var categoryRepo = require('../repos/categoryRepo');

module.exports = (req, res, next) => {

	if (req.session.isLogged === undefined) {
		req.session.isLogged = false;
    }
    if(req.session.isAdmin === undefined){
        req.session.isAdmin = false;
    }

    categoryRepo.loadAll().then(rows => {
        res.locals.layoutVM = {
            categories: rows,
            suppliers: rows,
            isLogged: req.session.isLogged,
            curUser: req.session.user,
            isAdmin: req.session.isAdmin
        };

        // console.log(res.locals.layoutVM.curUser);

        next();
    });
};