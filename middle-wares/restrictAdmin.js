module.exports = (req, res, next) => {
    
    if (req.session.isLogged === true && req.session.isAdmin !== undefined  && req.session.isAdmin !== false) {
        next();
    } else {
        res.redirect(`/account/login?retUrl=${req.originalUrl}`);
    }
}