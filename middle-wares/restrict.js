module.exports = (req, res, next) => {
    console.log(req.session.curKH);
    console.log(req.session.isLogged);
    if (req.session.isLogged === true && req.session.curKH !== undefined) {
        next();
    } else {
        res.redirect(`/account/login?retUrl=${req.originalUrl}`);
    }
}