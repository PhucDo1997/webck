var db = require('../fn/db');
SHA256 = require('crypto-js/sha256');

exports.add = user => {
    var sql = `insert into khachhang(ten, gioitinh, diachi, sodienthoai, matkhau, email) values('${user.ten}', '${user.gioitinh}', '${user.diachi}', '${user.sdt}', '${user.matkhau}', '${user.email}')`;
    return db.save(sql);
}

exports.login = user => {
    var sql = `select * from users where f_Username = '${user.username}' and f_Password = '${user.password}'`;
    return db.load(sql);
}

exports.isExistAccount = sdt =>{
    var sql = `select * from khachhang where sodienthoai = ${sdt}`;
    return db.load(sql);
}

exports.checkLoginUser = (sdt, pw) =>{
    console.log(pw);
    var mk = SHA256(pw).toString();
    console.log(mk);
    var sql = `select * from khachhang where sodienthoai = '${sdt}' and matkhau = '${mk}'`;
    return db.load(sql);
}

exports.checkLoginAdmin = (sdt, pw) =>{
    var mk = pw;
    
    var sql = `select * from admin where taikhoan = '${sdt}' and matkhau = '${mk}'`;
    return db.load(sql);
}

exports.update = user =>{
    var sql = `update khachhang set ten = '${user.ten}', gioitinh = '${user.gioitinh}', diachi = '${user.diachi}', sodienthoai = '${user.sodienthoai}', email = '${user.email}' where makh = '${user.makh}'`;
    return db.save(sql);
}

exports.getUser = maKH =>{
    var sql = `select * from khachhang where makh = '${maKH}'`;
    return db.load(sql);
}