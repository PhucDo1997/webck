var db = require('../fn/db');

exports.loadAllBill = (offset, maKH) => {
    var sql = `select * from donhang  where makh = ${maKH} order by ngaymua desc limit 5 offset ${offset}`;
    return db.load(sql);
}

exports.countAllBill = (maKH) => {
    var sql = `select count(*) as total from donhang where makh = ${maKH}`;
    return db.load(sql);
}

exports.loadSingleBill = (BillId) => {
    var sql = `select * from donhang where madon = '${BillId}'`;
    return db.load(sql);
}

exports.loadProductBill = (BillId) => {
    var sql = `select c.masp, s.linkanh, s.ten, h.ten as 'hangsx', c.soluong, (c.soluong * s.gia) as 'sotien'
                from donhang as d, chitietdonhang as c, sanpham as s, hangsanxuat as h 
                where d.madon = '${BillId}' && d.madon = c.madon && c.masp = s.masp && s.hangsx = h.mahangsx;`;
    return db.load(sql);
}

exports.loadCustomerBill = (BillId) => {
    var sql = `select k.ten, k.diachi, k.sodienthoai
    from donhang as d, khachhang as k
    where d.madon = '${BillId}' && d.makh = k.makh;`;
    return db.load(sql);
}

exports.insertBill = (billItem) => {
    var makh = billItem.makh;
    var tennguoinhan = billItem.tennguoinhan;
    var diachinhan = billItem.diachinhan;
    var sodienthoai = billItem.sodienthoai;
    var ghichu = billItem.ghichu;
    var ngaymua = billItem.ngaymua;
    var tinhtrang = 0;

    var sql = `INSERT INTO donhang(makh, tennguoinhan, diachinhan, sodienthoai, ghichu, ngaymua, tinhtrang) VALUES ('${makh}', '${tennguoinhan}', '${diachinhan}', '${sodienthoai}', '${ghichu}', '${ngaymua}', '${tinhtrang}');`;
    return db.save(sql);
}

exports.insertDetailBill = (detailBillItem) => {
    var madon = detailBillItem.madon;
    var sanpham = detailBillItem.sanpham;
    var result;
    for(var i = 0; i < sanpham.length; i++){
        var sql = `INSERT INTO chitietdonhang(madon, masp, soluong) VALUES ('${madon}', '${sanpham[i].masp}', '${sanpham[i].soluong}');`;
        result = db.save(sql);
    }
    return result;
}