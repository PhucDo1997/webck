var db = require('../fn/db');
var config = require('../config/config');

exports.loadAll = () => {
    var sql = 'select * from sanpham';
    return db.load(sql);
}

exports.loadBrand = () => {
    var sql = 'select * from hangsanxuat';
    return db.load(sql);
}

// exports.loadAllByCat = (catId) => {
//     var sql = `select * from products where CatID = ${catId}`;
//     return db.load(sql);
// }
exports.loadNewProduct = () => {
    //chua fix
    //console.log(product);
    //return product;
    var sql = `select datediff(curdate(), ngaynhaphang) as songay, s.masp, s.ten, gia, linkanh, h.ten as brand
    from sanpham s, hangsanxuat h
    where s.hangsx = h.mahangsx
    order by songay limit 10`;
    return db.load(sql);
}
exports.loadCommonProduct = () => {//xem nhiều
    var sql = `select s.masp, luotxem , s.ten, gia, linkanh, h.ten as brand
    from sanpham s, hangsanxuat h
    where s.hangsx = h.mahangsx 
    order by luotxem desc limit 10`;
    return db.load(sql);
}

exports.loadSellerProduct = () => {//bán chạy
    var sql = `select s.masp, soluongban as bestseller, s.ten, gia, linkanh, h.ten as brand
    from sanpham s, hangsanxuat h
    where s.hangsx = h.mahangsx 
    order by bestseller desc limit 10`;
    return db.load(sql);
}

exports.loadBestSellerProduct = () => {//bán tốt nhất / ngày
    //chua fix
    //console.log(product);
    //return product;
    var sql = `select s.masp, soluongban / datediff(curdate(), ngaynhaphang) as bestseller, s.ten, gia, linkanh, h.ten as brand
    from sanpham s, hangsanxuat h
    where s.hangsx = h.mahangsx 
    order by bestseller desc limit 1`;
    return db.load(sql);
    //var sql = `select * from products where CatID = ${catId} limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
    //return db.load(sql);
}
exports.loadAllByCat = (catId, offset) => {
    var id;

    if (catId == 'bag') id = 2;
    else if (catId == 'balo') id = 1;
    var sql = `select s.masp, s.ten, s.linkanh, s.gia, h.ten as brand from sanpham s, hangsanxuat h where s.hangsx = h.mahangsx and loai = ${id} limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
    //return product;
    return db.load(sql);
}

exports.loadAllByCatBrand = (catId, offset, brandId) => {
    var id;
    if (catId == 'bag') id = 2;
    else if (catId == 'balo') id = 1;
    var sql = `select s.masp, s.ten, s.linkanh, s.gia, h.ten as brand from sanpham s, hangsanxuat h where s.hangsx = h.mahangsx and loai = ${id} and s.hangsx = ${brandId} limit ${config.PRODUCTS_PER_PAGE} offset ${offset}`;
    //return product;
    return db.load(sql);
}

exports.countByCat = catId => {
    var id;
    if (catId == 'bag') id = 2;
    if (catId == 'balo') id = 1;
    var sql = `select count(*) as total from sanpham where loai = ${id}`;
    //return product;
    return db.load(sql);
}

exports.countByCatBrand = (catId, brandId) => {
    var id;
    if (catId == 'bag') id = 2;
    if (catId == 'balo') id = 1;
    var sql = `select count(*) as total from sanpham where loai = ${id} and hangsx = ${brandId}`;
    //return product;
    return db.load(sql);
}

exports.single = proId => {
    //var sql = `select * from sanpham where masp = ${proId}`;
    var sql = `select s.soluongcon, s.masp, s.ten, s.linkanh, s.gia, h.ten as brand from sanpham s, hangsanxuat h where s.masp = ${proId}`;
    return db.load(sql);
}

exports.loadSameBrand = proID =>{
    var sql = `select b.masp, b.linkanh from sanpham a, sanpham b where a.masp = ${proID} and a.hangsx = b.hangsx limit 5`;
    return db.load(sql);
}

exports.loadSameType = proID =>{
    var sql = `select b.masp, b.linkanh from sanpham a, sanpham b where a.masp = ${proID} and a.loai = b.loai limit 5`;
    return db.load(sql);
}

exports.search = (type, search, offset) => {
    var sql;
    switch (type) {
        case "0":
            sql = `select s.masp, s.ten, s.linkanh, s.gia, h.ten as brand
                    from sanpham s, hangsanxuat h
                    where s.hangsx = h.mahangsx and s.ten like '%${search}%' limit ${config.PRODUCTS_PER_PAGE} offset ${offset};`;
            break;
        case "1":
            sql = `select s.masp, s.ten, s.linkanh, s.gia, h.ten as brand
            from sanpham as s, hangsanxuat as h 
            where s.hangsx = h.mahangsx and h.ten like '%${search}%' limit ${config.PRODUCTS_PER_PAGE} offset ${offset};`;
            break;
    }
    //console.log(sql);
    return db.load(sql);
}

exports.countBySearch = (type, search) => {
    var sql;
    switch (type) {
        case "0":
            sql = `select count(*) as total
                    from sanpham
                    where ten like '%${search}%';`;
            break;
        case "1":
        sql = `select count(*) as total
                from sanpham
                where ten like '%${search}%';`;
            break;
    }
    return db.load(sql);
}

exports.updateSoLuongCon = (makh, soluongcon) => {
    var sql = `update sanpham set soluongcon = ${soluongcon} where makh = ${makh}`;
    return db.save(sql);
}