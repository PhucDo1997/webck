var db = require('../fn/db');

exports.loadAllBill = (offset) => {
    var sql = `select * from donhang order by ngaymua desc limit 5 offset ${offset}`;
    return db.load(sql);
}

exports.countAllBill = () => {
    var sql = `select count(*) as total from donhang`;
    return db.load(sql);
}

exports.loadSingleBill = (BillId) => {
    var sql = `select * from donhang where madon = '${BillId}'`;
    return db.load(sql);
}

exports.loadProductBill = (BillId) => {
    var sql = `select c.masp, s.linkanh, s.ten, h.ten as 'hangsx', c.soluong, (c.soluong * s.gia) as 'sotien'
                from donhang as d, chitietdonhang as c, sanpham as s, hangsanxuat as h 
                where d.madon = '${BillId}' && d.madon = c.madon && c.masp = s.masp && s.hangsx = h.mahangsx;`;
    return db.load(sql);
}

exports.loadCustomerBill = (BillId) => {
    var sql = `select k.ten, k.diachi, k.sodienthoai
    from donhang as d, khachhang as k
    where d.madon = '${BillId}' && d.makh = k.makh;`;
    return db.load(sql);
}

exports.changeStatus = (BillId, Status) => {
    var sql = `update donhang set tinhtrang = ${Status} where madon = '${BillId}'`;
    return db.save(sql);
}

exports.soLuongBanByCat = (catId) => {
    var sql = `select sum(soluongban) total
	            from sanpham
                where loai = '${catId}';`;
    return db.load(sql);
}

exports.getToDaySale = () => {
    var sql = `select sum(c.soluong) as total
                from donhang as d, chitietdonhang as c
                where date(d.ngaymua) = curdate() and d.madon = c.madon;`;
    return db.load(sql);
}

exports.getToDayBill = () => {
    var sql = `select count(*) as total
                from donhang
                where date(ngaymua) = curdate();`;
    return db.load(sql);
}

exports.getTotalSale = () => {
    var sql = `select sum(soluong) as total
                from chitietdonhang;`;
    return db.load(sql);
}

exports.getTotalBill = () => {
    var sql = `select count(*) as total
                from donhang;`;
    return db.load(sql);
}